package pl.sdacademy.jdbc.db;

import java.util.Set;

import pl.sdacademy.jdbc.person.Person;
import pl.sdacademy.jdbc.person.PersonDAO;

public class Application {

	public static void main(String[] args) {

		// Long id = 1L;
		// PersonDAO personDAO = new PersonDAO();
		// Optional<Person> optionalPerson = personDAO.findById(id);
		//
		// if (optionalPerson.isPresent()) {
		// System.out.println(optionalPerson.get());
		// } else {
		// System.out.println("Brak osoby o id" + id);
		// }

		PersonDAO personDAO = new PersonDAO();
		Set<Person> all = personDAO.findAll();

		for (Person person : all) {
			System.out.println(person);
		}

	}

}
