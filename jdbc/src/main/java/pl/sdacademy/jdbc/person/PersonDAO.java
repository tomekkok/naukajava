package pl.sdacademy.jdbc.person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import pl.sdacademy.jdbc.db.DBUtil;

/**
 *
 */
public class PersonDAO { // Data Access Object

	private final Connection connection;

	public PersonDAO() {
		this.connection = DBUtil.getConnection();
	}

	public Optional<Person> findById(Long userId) {
		// 1) PreparedStatement
		// 2) executeQuery()
		// 3) next()
		// 4) new Person() || empty()

		// Optional.of(new Person()) istnieje
		// Optional.empty() nie istnieje
		// Statement statement = connection.createStatement();
		// String sql = "SELECT id, first_name, last_name FROM people";
		// ResultSet result = statement.executeQuery(sql);

		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("SELECT id, first_name, last_name FROM person WHERE id = ?");
			preparedStatement.setLong(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				Person person = new Person();
				person.setId(resultSet.getLong("id"));
				person.setFirstName(resultSet.getString("first_name"));
				person.setLastName(resultSet.getString("last_name"));
				return Optional.of(person);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Optional.empty();
	}

	public Set<Person> findAll() {
		PreparedStatement preparedStatement;

		Set<Person> persons = new HashSet<>();
		try {
			preparedStatement = connection.prepareStatement("SELECT id, first_name, last_name FROM person");
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Person person = new Person();
				person.setId(resultSet.getLong("id"));
				person.setFirstName(resultSet.getString("first_name"));
				person.setLastName(resultSet.getString("last_name"));
				persons.add(person);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return persons;

	}

	public void addPerson(Person person) {

	}

	public void deletePerson(Long personId) {

	}

	public void updatePerson(Person person) {

	}
}
