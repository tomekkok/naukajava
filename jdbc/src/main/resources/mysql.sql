CREATE SCHEMA `jdbc_test` ;
use `jdbc_test` ;

CREATE TABLE `jdbc_test`.`person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));

CREATE USER user_jdbc IDENTIFIED BY 'jdbc01';

GRANT USAGE ON *.* TO user_jdbc@localhost identified BY 'jdbc01';
GRANT ALL PRIVILEGES ON jdbc_test.* TO user_jdbc@localhost;