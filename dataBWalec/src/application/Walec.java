package application;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Walec {

	private DoubleProperty r = new SimpleDoubleProperty();
	private DoubleProperty h = new SimpleDoubleProperty();

	public final DoubleProperty rProperty() {
		return this.r;
	}

	public final double getR() {
		return this.rProperty().get();
	}

	public final void setR(final double r) {
		this.rProperty().set(r);
	}

	public final DoubleProperty hProperty() {
		return this.h;
	}

	public final double getH() {
		return this.hProperty().get();
	}

	public final void setH(final double h) {
		this.hProperty().set(h);
	}

	public DoubleBinding polePowierzchni() {

		return new DoubleBinding() {
			{
				bind(r, h);
			}

			@Override
			protected double computeValue() {

				return 2 * Math.PI * r.get() * (r.get() + h.get());
			}
		};
	}

	public DoubleExpression objetosc() {
		return new DoubleBinding() {
			{
				bind(r, h);
			}

			@Override
			protected double computeValue() {

				return Math.PI * r.get() * r.get() * h.get();
			}
		};
	}

}
