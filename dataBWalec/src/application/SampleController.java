package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

public class SampleController implements Initializable {

	@FXML
	private TextField promienTF;

	@FXML
	private TextField wysokoscTF;

	@FXML
	private TextField polePowTF;

	@FXML
	private TextField objetoscTF;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		Walec w = new Walec();

		promienTF.textProperty().bindBidirectional(w.rProperty(), new NumberStringConverter());
		wysokoscTF.textProperty().bindBidirectional(w.hProperty(), new NumberStringConverter());

		polePowTF.textProperty().bind(w.polePowierzchni().asString());
		objetoscTF.textProperty().bind(w.objetosc().asString());

	}

}
