package application;

import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Obliczenia {

	private DoubleProperty a = new SimpleDoubleProperty();
	private DoubleProperty b = new SimpleDoubleProperty();

	public final DoubleProperty aProperty() {
		return this.a;
	}

	public final double getA() {
		return this.aProperty().get();
	}

	public final void setA(final double a) {
		this.aProperty().set(a);
	}

	public final DoubleProperty bProperty() {
		return this.b;
	}

	public final double getB() {
		return this.bProperty().get();
	}

	public final void setB(final double b) {
		this.bProperty().set(b);
	}

	public DoubleExpression add() {
		return a.add(b);
	}

	public DoubleExpression substract() {
		return a.subtract(b);
	}

	public DoubleExpression divide() {
		return a.divide(b);
	}

	public DoubleExpression multiply() {
		return a.multiply(b);
	}

}
