package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

public class SampleController implements Initializable {

	@FXML
	private TextField LiczbaA;
	@FXML
	private TextField LiczbaB;
	@FXML
	private TextField wynikDodawania;
	@FXML
	private TextField wynikOdejmowania;
	@FXML
	private TextField wynikMnozenia;
	@FXML
	private TextField wynikDzielenia;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		Obliczenia o = new Obliczenia();
		LiczbaA.textProperty().bindBidirectional(o.aProperty(), new NumberStringConverter());
		LiczbaB.textProperty().bindBidirectional(o.bProperty(), new NumberStringConverter());

		wynikDodawania.textProperty().bind(o.add().asString());

		wynikOdejmowania.textProperty().bind(o.substract().asString());

		wynikMnozenia.textProperty().bind(o.multiply().asString());

		wynikDzielenia.textProperty().bind(o.divide().asString());
	}
}
