package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main2 extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = createGUI();

			Scene scene = new Scene(root, 400, 400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	private BorderPane createGUI() {

		BorderPane root = new BorderPane();
		MenuBar menuBar = new MenuBar();

		Menu file = new Menu("File");
		MenuItem openFile = new MenuItem("Open File");
		openFile.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Informacja");
				alert.setHeaderText("Informacja");
				alert.setContentText("Mam informacje dla Ciebie!");

				alert.showAndWait();

			}
		});
		MenuItem saveFile = new MenuItem("Save File");
		MenuItem exitApp = new MenuItem("Exit");
		exitApp.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Platform.exit();

			}
		});

		file.getItems().addAll(openFile, saveFile, exitApp);

		Menu edit = new Menu("Edit");
		Menu help = new Menu("Help");

		menuBar.getMenus().addAll(file, edit, help);
		root.setTop(menuBar);

		return root;

	}
}
