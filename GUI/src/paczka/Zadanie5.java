package paczka;

import java.awt.EventQueue;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Zadanie5 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Zadanie5 window = new Zadanie5();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Zadanie5() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setBounds(24, 70, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel label = new JLabel("+");
		label.setBounds(118, 73, 17, 14);
		frame.getContentPane().add(label);

		textField_1 = new JTextField();
		textField_1.setBounds(145, 70, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		textField_1.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {

			}

			public void focusLost(FocusEvent e) {

				try {
					double a = Double.parseDouble(textField.getText());
					double b = Double.parseDouble(textField_1.getText());
					double c = a + b;
					textField_2.setText(String.valueOf(c));
				} catch (NumberFormatException exception) {
					textField_2.setText("?");

				}

			}
		});

		JLabel label_1 = new JLabel("=");
		label_1.setBounds(241, 73, 22, 14);
		frame.getContentPane().add(label_1);

		textField_2 = new JTextField();
		textField_2.setBounds(285, 70, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);

	}

}
