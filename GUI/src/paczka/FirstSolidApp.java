package paczka;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class FirstSolidApp {
	// ten sposob aby nie blokowac watku

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					JFrame frame = new JFrame();
					frame.setBounds(100, 100, 450, 300);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();
				}

			}

		});

	}

}
