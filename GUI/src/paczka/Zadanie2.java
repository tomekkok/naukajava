package paczka;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Zadanie2 {

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					JFrame frame = new JFrame();
					frame.setBounds(100, 100, 450, 300);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setVisible(true);
					frame.setLayout(null);
					frame.setTitle("tytu� 1");
					JLabel label = new JLabel();
					label.setBounds(20, 75, 150, 20);

					JButton button = new JButton("OK");
					button.setBounds(20, 100, 150, 20);
					frame.add(button);
					button.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							frame.setTitle("Tytu� 2");
						}
					});

				} catch (Exception e) {

					e.printStackTrace();
				}

			}

		});

	}
}