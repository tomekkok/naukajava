package paczka;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Zadanie3 {

	private JFrame frmZadanie;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Zadanie3 window = new Zadanie3();
					window.frmZadanie.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Zadanie3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmZadanie = new JFrame();
		frmZadanie.setTitle("Zadanie 3");
		frmZadanie.setBounds(100, 100, 450, 300);
		frmZadanie.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmZadanie.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setBounds(62, 84, 86, 20);
		frmZadanie.getContentPane().add(textField);
		textField.setColumns(10);
		textField.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {

			}

			public void focusLost(FocusEvent e) {
				double a = Double.parseDouble(textField.getText());
				textField_1.setText(String.valueOf(-a));
			}
		});

		textField_1 = new JTextField();
		textField_1.setBounds(211, 84, 86, 20);
		frmZadanie.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		JButton btnNewButton = new JButton("Przenie\u015B");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double a = Double.parseDouble(textField.getText());
				textField_1.setText(String.valueOf(-a));

			}
		});
		btnNewButton.setBounds(159, 161, 89, 23);
		frmZadanie.getContentPane().add(btnNewButton);
	}
}
