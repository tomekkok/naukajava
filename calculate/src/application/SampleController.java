package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class SampleController {

	@FXML

	private TextField myTextField1;
	private TextField myTextField2;
	private TextField myTextFieldWynik;
	private Button BDodawanie;
	private Button BOdejmowanie;
	private Button BMnozenie;
	private Button BDzielenie;

	public void wcisnietoDodawanie(ActionEvent event) {
		double a = Double.parseDouble(myTextField1.getText());
		double b = Double.parseDouble(myTextField2.getText());

		double wynik = a + b;
		myTextFieldWynik.setText(String.valueOf(wynik));
	}

	public void wcisnietoOdejmowanie(ActionEvent event) {
		double a = Double.parseDouble(myTextField1.getText());
		double b = Double.parseDouble(myTextField2.getText());

		double wynik = a - b;
		myTextFieldWynik.setText(String.valueOf(wynik));
	}

	public void wcisnietoMnozenie(ActionEvent event) {
		double a = Double.parseDouble(myTextField1.getText());
		double b = Double.parseDouble(myTextField2.getText());

		double wynik = a * b;
		myTextFieldWynik.setText(String.valueOf(wynik));
	}

	public void wcisnietoDzielenie(ActionEvent event) {
		double a = Double.parseDouble(myTextField1.getText());
		double b = Double.parseDouble(myTextField2.getText());

		double wynik = a / b;
		myTextFieldWynik.setText(String.valueOf(wynik));
	}
}
