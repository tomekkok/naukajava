package sample;

public class InvestmentImpl implements Investment {
	
	private double interest;
	private int months;
	private int dolarsAmount;
	private double tax;

	@Override
	public void setInterest(double interest) {
		this.interest = interest;
	}

	@Override
	public void setTax(double tax) {
		this.tax = tax;
	}

	@Override
	public void setAmount(int dolarsAmount) {
		this.dolarsAmount = dolarsAmount;
	}

	@Override
	public void setPeriod(int months) {
		this.months = months;
	}

	@Override
	public int countProfit() throws IllegalArgumentException {
		
		
		return countAccountBalance() - dolarsAmount ;
	}

	@Override
	public int countAccountBalance() throws IllegalArgumentException {
		if (dolarsAmount < 0) {
			throw new IllegalArgumentException("dolarsAmount < 0");
		}
		if (months < 0 ){
			throw new IllegalArgumentException("months < 0");
		}
		if (tax < 0 || tax > 1){
			throw new IllegalArgumentException("tax < 0 or tax > 1");
		}
		if (interest < 0)
			throw new IllegalArgumentException("interest < 0");
		
		double interestPerMonth = interest/12;
		
		double newAmount = dolarsAmount;
		
		for(int i = 0 ; i < months ; i++){
			newAmount *= (1+interestPerMonth*(1-tax));
		}
		return (int) newAmount;
	}

	
	public static void main(String[] args) {
		
		Investment investment = new InvestmentImpl();
		investment.setAmount(10000);
		investment.setInterest(0.12f);
		investment.setTax(0f);
		investment.setPeriod(1);
		
		try {
			System.out.println(investment.countAccountBalance());
			System.out.println(investment.countProfit());
			investment.setPeriod(12);
			System.out.println(investment.countAccountBalance());
			System.out.println(investment.countProfit());
			investment.setTax(0.25f);

			System.out.println(investment.countAccountBalance());
			System.out.println(investment.countProfit());
			
		} catch (IllegalArgumentException e) {
			// TODO: handle exception
		}
		
		try{
			investment.setInterest(-200);
			investment.countProfit();
		}catch(IllegalArgumentException argumentException){
			System.err.println(argumentException.getMessage());
		}

		investment.setInterest(0.1);
		
		try{
			investment.setTax(-1);
			investment.countProfit();
		}catch(IllegalArgumentException argumentException){
			System.err.println(argumentException.getMessage());
		}

		investment.setTax(0.1);
		
		try{
			investment.setPeriod(-10);
			investment.countProfit();
		}catch(IllegalArgumentException argumentException){
			System.err.println(argumentException.getMessage());
		}
		

		investment.setPeriod(12);
		
		try{
			investment.setAmount(-10);
			investment.countProfit();
		}catch(IllegalArgumentException argumentException){
			System.err.println(argumentException.getMessage());
		}
		
	}
}
