package sample;

public class InvestmentTest implements Investment {

    private double interest;
    private double tax;
    private int dolarsAmount;
    private int months;
    private int result;

    @Override
    public void setInterest(double interest) {
        System.out.println("interest = " + interest);
        this.interest = interest;
    }

    @Override
    public void setTax(double tax) {
        System.out.println("tax = " + tax);
        this.tax = tax;
    }

    @Override
    public void setAmount(int dolarsAmount) {
        System.out.println("dolarsAmount = " + dolarsAmount);
        this.dolarsAmount = dolarsAmount;
    }

    @Override
    public void setPeriod(int months) {
        System.out.println("months = " + months);
        this.months = months;
    }

    @Override
    public int countProfit() throws IllegalArgumentException {
        return 90;

    }

    @Override
    public int countAccountBalance() throws IllegalArgumentException {
        return 780;
    }
}
