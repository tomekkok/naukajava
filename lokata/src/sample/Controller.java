package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class Controller {
    private Investment invest = new InvestmentTest();
    //private Investment invest = new InvestmentImpl();

    @FXML
    private TextField interestRate;

    @FXML
    private TextField taxRate;

    @FXML
    private TextField cashToStartWith;

    @FXML
    private TextField timePeriod;

    @FXML
    private TextField profitCount;

    @FXML
    private TextField accBalance;

    @FXML
    void TotalUp(ActionEvent event) {
        double interest = Double.parseDouble(interestRate.getText());
        double tax = Double.parseDouble(taxRate.getText());
        int dolarsAmount = Integer.parseInt(cashToStartWith.getText());
        int months = Integer.parseInt(timePeriod.getText());
        invest.setInterest(interest/100);
        invest.setTax(tax/100);
        invest.setAmount(dolarsAmount);
        invest.setPeriod(months);

        profitCount.setText(Integer.toString(invest.countProfit()));
        accBalance.setText(Integer.toString(invest.countAccountBalance()));
    }
}
