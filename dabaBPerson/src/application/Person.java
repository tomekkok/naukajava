package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {

	private StringProperty name = new SimpleStringProperty();
	private StringProperty surname = new SimpleStringProperty();
	private IntegerProperty age = new SimpleIntegerProperty();
	private StringProperty phone = new SimpleStringProperty();
	private StringProperty mail = new SimpleStringProperty();

	public final StringProperty nameProperty() {
		return this.name;
	}

	public final String getName() {
		return this.nameProperty().get();
	}

	public final void setName(final String name) {
		this.nameProperty().set(name);
	}

	public final StringProperty surnameProperty() {
		return this.surname;
	}

	public final String getSurname() {
		return this.surnameProperty().get();
	}

	public final void setSurname(final String surname) {
		this.surnameProperty().set(surname);
	}

	public final IntegerProperty ageProperty() {
		return this.age;
	}

	public final int getAge() {
		return this.ageProperty().get();
	}

	public final void setAge(final int age) {
		this.ageProperty().set(age);
	}

	public final StringProperty phoneProperty() {
		return this.phone;
	}

	public final String getPhone() {
		return this.phoneProperty().get();
	}

	public final void setPhone(final String phone) {
		this.phoneProperty().set(phone);
	}

	public final StringProperty mailProperty() {
		return this.mail;
	}

	public final String getMail() {
		return this.mailProperty().get();
	}

	public final void setMail(final String mail) {
		this.mailProperty().set(mail);
	}

	@Override
	public String toString() {
		return "Person [name=" + name.get() + ", surname=" + surname.get() + ", age=" + age.get() + ", phone="
				+ phone.get() + ", mail=" + mail.get() + "]";
	}

	public void save() {
		try {
			PrintStream out = new PrintStream("person.txt");
			out.println(name.get());
			out.println(surname.get());
			out.println(age.get());
			out.println(phone.get());
			out.println(mail.get());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void open() {
		// Scanner in;

		Scanner scanner = null;
		try {
			scanner = new Scanner(new File("person.txt"));
			name.set(scanner.nextLine());
			surname.set(scanner.nextLine());
			age.set(scanner.nextInt());
			phone.set(scanner.nextLine());
			mail.set(scanner.nextLine());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scanner != null)
				scanner.close();
		}

	}
}
