package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

public class SampleController implements Initializable {
	private Person person = new Person();

	@FXML
	private TextField nameTF;

	@FXML
	private TextField surnameTF;

	@FXML
	private TextField ageTF;

	@FXML
	private TextField phoneTF;

	@FXML
	private TextField mailTF;

	@FXML
	void showBT(ActionEvent event) {
		System.out.println(person.toString());
	}

	@FXML
	void resetBT(ActionEvent event) {
		person.setName("-");
		person.setSurname("-");
		person.setAge(0);
		person.setPhone("-");
		person.setMail("-");
	}

	@FXML
	void saveToFile(ActionEvent event) {
		person.save();
	}

	@FXML
	void openFromFile(ActionEvent event) {
		person.open();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		nameTF.textProperty().bindBidirectional(person.nameProperty());
		surnameTF.textProperty().bindBidirectional(person.surnameProperty());
		ageTF.textProperty().bindBidirectional(person.ageProperty(), new NumberStringConverter());
		phoneTF.textProperty().bindBidirectional(person.phoneProperty());
		mailTF.textProperty().bindBidirectional(person.mailProperty());
	}

}
