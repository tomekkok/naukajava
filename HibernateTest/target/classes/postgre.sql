--
-- PostgreSQL database dump
--
CREATE DATABASE hibernate_test WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pl_PL.UTF-8' LC_CTYPE = 'pl_PL.UTF-8';
ALTER DATABASE hibernate_test OWNER TO postgres;



SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: school; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE IF NOT EXISTS school (
    id bigint NOT NULL,
    level integer NOT NULL,
    name character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    zip_code character varying(255) NOT NULL,
    street character varying(255) NOT NULL
);


ALTER TABLE school OWNER TO postgres;

--
-- Name: school_class; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE IF NOT EXISTS school_class (
    id bigint NOT NULL,
    level integer NOT NULL,
    name character varying(255) NOT NULL,
    school_id bigint NOT NULL
);


ALTER TABLE school_class OWNER TO postgres;

--
-- Name: school_class_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE school_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE school_class_id_seq OWNER TO postgres;

--
-- Name: school_class_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE school_class_id_seq OWNED BY school_class.id;


--
-- Name: school_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE school_id_seq OWNER TO postgres;

--
-- Name: school_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE school_id_seq OWNED BY school.id;


--
-- Name: student; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE IF NOT EXISTS student (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    reg_number integer NOT NULL,
    school_class_id bigint NOT NULL
);


ALTER TABLE student OWNER TO postgres;

--
-- Name: student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE student_id_seq OWNER TO postgres;

--
-- Name: student_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_id_seq OWNED BY student.id;


--
-- Name: student_test; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE IF NOT EXISTS student_test (
    id bigint NOT NULL,
    student_id bigint NOT NULL,
    test_id bigint NOT NULL,
	score DOUBLE PRECISION NOT NULL
);


ALTER TABLE student_test OWNER TO postgres;

--
-- Name: student_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE student_test_id_seq OWNER TO postgres;

--
-- Name: student_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_test_id_seq OWNED BY student_test.id;


--
-- Name: test; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE IF NOT EXISTS test (
    id bigint NOT NULL,
    end_date timestamp without time zone NOT NULL
);


ALTER TABLE test OWNER TO postgres;

--
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_id_seq OWNER TO postgres;

--
-- Name: test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_id_seq OWNED BY test.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY school ALTER COLUMN id SET DEFAULT nextval('school_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY school_class ALTER COLUMN id SET DEFAULT nextval('school_class_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student ALTER COLUMN id SET DEFAULT nextval('student_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test ALTER COLUMN id SET DEFAULT nextval('student_test_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test ALTER COLUMN id SET DEFAULT nextval('test_id_seq'::regclass);


--
-- Data for Name: play_evolutions; Type: TABLE DATA; Schema: public; Owner: postgres
--

--
-- Data for Name: school; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- Data for Name: school_class; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- Name: school_class_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('school_class_id_seq', 1, false);


--
-- Name: school_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('school_id_seq', 1, false);


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: postgres
--

--
-- Name: student_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_id_seq', 1, false);


--
-- Data for Name: student_test; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- Name: student_test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_test_id_seq', 1, false);


--
-- Data for Name: test; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- Name: test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('test_id_seq', 1, false);


--
-- Name: pk_school; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY school ADD CONSTRAINT pk_school PRIMARY KEY (id);


--
-- Name: pk_school_class; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY school_class
    ADD CONSTRAINT pk_school_class PRIMARY KEY (id);


--
-- Name: pk_student; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY student
    ADD CONSTRAINT pk_student PRIMARY KEY (id);


--
-- Name: pk_student_test; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY student_test
    ADD CONSTRAINT pk_student_test PRIMARY KEY (id);


--
-- Name: pk_test; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY test
    ADD CONSTRAINT pk_test PRIMARY KEY (id);



--
-- Name: ix_school_class_school_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace:
--

CREATE INDEX ix_school_class_school_1 ON school_class USING btree (school_id);


--
-- Name: ix_student_schoolclass_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace:
--

CREATE INDEX ix_student_schoolclass_2 ON student USING btree (school_class_id);


--
-- Name: ix_student_test_student_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace:
--

CREATE INDEX ix_student_test_student_3 ON student_test USING btree (student_id);


--
-- Name: ix_student_test_test_4; Type: INDEX; Schema: public; Owner: postgres; Tablespace:
--

CREATE INDEX ix_student_test_test_4 ON student_test USING btree (test_id);


--
-- Name: fk_school_class_school_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY school_class
    ADD CONSTRAINT fk_school_class_school_1 FOREIGN KEY (school_id) REFERENCES school(id);


--
-- Name: fk_student_schoolclass_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student
    ADD CONSTRAINT fk_student_schoolclass_2 FOREIGN KEY (school_class_id) REFERENCES school_class(id);


--
-- Name: fk_student_test_student_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test
    ADD CONSTRAINT fk_student_test_student_3 FOREIGN KEY (student_id) REFERENCES student(id);


--
-- Name: fk_student_test_test_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test
    ADD CONSTRAINT fk_student_test_test_4 FOREIGN KEY (test_id) REFERENCES test(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

