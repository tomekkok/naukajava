CREATE DATABASE hibernate_test WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pl_PL.UTF-8' LC_CTYPE = 'pl_PL.UTF-8';
ALTER DATABASE hibernate_test OWNER TO postgres;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE TABLE IF NOT EXISTS school (
    id bigint NOT NULL,
    level integer NOT NULL,
    name character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    zip_code character varying(255) NOT NULL,
    street character varying(255) NOT NULL
);

ALTER TABLE school OWNER TO postgres;

CREATE TABLE IF NOT EXISTS school_class (
    id bigint NOT NULL,
    level integer NOT NULL,
    name character varying(255) NOT NULL,
    school_id bigint NOT NULL
);

ALTER TABLE school_class OWNER TO postgres;

CREATE SEQUENCE school_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE school_class_id_seq OWNER TO postgres;
ALTER SEQUENCE school_class_id_seq OWNED BY school_class.id;

CREATE SEQUENCE school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE school_id_seq OWNER TO postgres;
ALTER SEQUENCE school_id_seq OWNED BY school.id;

CREATE TABLE IF NOT EXISTS student (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    reg_number integer NOT NULL,
    school_class_id bigint NOT NULL
);

CREATE SEQUENCE student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE student_id_seq OWNER TO postgres;
ALTER SEQUENCE student_id_seq OWNED BY student.id;

SELECT pg_catalog.setval('school_id_seq', 0, false);
SELECT pg_catalog.setval('school_class_id_seq', 0, false);
SELECT pg_catalog.setval('student_id_seq', 0, false);


ALTER TABLE ONLY school ALTER COLUMN id SET DEFAULT nextval('school_id_seq'::regclass);
ALTER TABLE ONLY school_class ALTER COLUMN id SET DEFAULT nextval('school_class_id_seq'::regclass);
ALTER TABLE ONLY student ALTER COLUMN id SET DEFAULT nextval('student_id_seq'::regclass);

ALTER TABLE ONLY school ADD CONSTRAINT pk_school PRIMARY KEY (id);

ALTER TABLE ONLY school_class
    ADD CONSTRAINT pk_school_class PRIMARY KEY (id);

ALTER TABLE ONLY student
    ADD CONSTRAINT pk_student PRIMARY KEY (id);


CREATE INDEX ix_school_class_school_1 ON school_class USING btree (school_id);

CREATE INDEX ix_student_schoolclass_2 ON student USING btree (school_class_id);

ALTER TABLE ONLY school_class
    ADD CONSTRAINT fk_school_class_school_1 FOREIGN KEY (school_id) REFERENCES school(id);

ALTER TABLE ONLY student
    ADD CONSTRAINT fk_student_schoolclass_2 FOREIGN KEY (school_class_id) REFERENCES school_class(id);

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
