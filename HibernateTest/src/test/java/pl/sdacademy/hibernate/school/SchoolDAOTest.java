package pl.sdacademy.hibernate.school;

import org.hibernate.Hibernate;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

/**
 *
 */
public class SchoolDAOTest {

	private SchoolDAO schoolDAO = new SchoolDAO();

	@Test
	public void schoolScenario() throws Exception {
		// create
		School school = createSchool();
		assertThat(school.getId(), is(nullValue()));

		// persist
		School savedSchool = schoolDAO.save(school);
		assertThat(school.getId(), is(not(nullValue())));

		// update
		assertThat(savedSchool.getLevel(), is(1));
		savedSchool.setLevel(3);
		schoolDAO.update(savedSchool);
		School fromDBSchool = schoolDAO.findById(savedSchool.getId());
		assertThat(fromDBSchool.getLevel(), is(3));

		// delete
		schoolDAO.delete(fromDBSchool);
		School fromDBDeletedSchool = schoolDAO.findById(fromDBSchool.getId());
		assertThat(fromDBDeletedSchool, is(nullValue()));
	}

	public static School createSchool() {
		School school = new School();
		school.setCity("Gdansk");
		school.setCountry("Polska");
		school.setLevel(1);
		school.setName("Szkola Podstawowa nr 36");
		school.setZipCode("80-321");
		school.setStreet("Uczniowska 17");
		return school;
	}
}