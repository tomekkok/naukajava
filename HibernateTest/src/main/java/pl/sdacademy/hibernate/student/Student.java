package pl.sdacademy.hibernate.student;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import pl.sdacademy.hibernate.school.SchoolClass;
import pl.sdacademy.hibernate.utils.SessionFactoryManager;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Paweł on 2016-03-20.
 */
@Entity
@Table(name = "student")
public class Student {

	@Id
	@SequenceGenerator(name = "pk_student", sequenceName = "student_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_student")
	private Long id;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "reg_number", nullable = false)
	private Integer regNumber;

	@ManyToOne(optional = false)
	@JoinColumn(name = "school_class_id")
	private SchoolClass schoolClass;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(Integer regNumber) {
		this.regNumber = regNumber;
	}

	public SchoolClass getSchoolClass() {
		return schoolClass;
	}

	public void setSchoolClass(SchoolClass schoolClass) {
		this.schoolClass = schoolClass;
	}

	public static List<Student> findByShoolClassId(long schoolClassId) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();

		Criteria criteria = session.createCriteria(Student.class);
		criteria.add(Restrictions.eq("schoolClass.id", schoolClassId));

		List<Student> results = (List<Student>) criteria.list();
		session.close();
		return results;
	}

	public static List<Student> findByShoolId(long schoolId) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();

		Criteria criteria = session.createCriteria(Student.class);
		criteria
				.createAlias("schoolClass", "sc")
				.createAlias("sc.school", "s")
				.add(Restrictions.eq("s.id", schoolId));

		List<Student> results = (List<Student>) criteria.list();
		session.close();
		return results;
	}

	public static Student findById(long id) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Student student = session.get(Student.class, id);
		session.close();
		return student;
	}

	public static Student save(Student student) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			student.id = null;
			session.save(student);
			transaction.commit();
			return student;
		} catch (Exception e) {
			System.out.println("Student - rollback");
			System.out.println(e.getMessage());
			transaction.rollback();
		} finally {
			session.close();
		}
		return null;
	}

	public static void update(Student student) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(student);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		} finally {
			session.close();
		}
	}

	public static void delete(Student student) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(student);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		} finally {
			session.close();
		}
	}
}
