package pl.sdacademy.hibernate.student;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sdacademy.hibernate.utils.SessionFactoryManager;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Paweł on 2016-03-20.
 */
@Entity
@Table(name = "test")
public class Test {

    @Id
    @SequenceGenerator(name = "pk_test", sequenceName = "test_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_test")
    private Long id;

    @Column(nullable = false, name = "end_date")
    private Date endDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public static Test save(Test test) {
        SessionFactory factory = SessionFactoryManager.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            test.id = null;
            session.save(test);
            transaction.commit();
            return test;
        } catch (Exception e) {
            System.out.println("Test - rollback");
            System.out.println(e.getMessage());
            transaction.rollback();
        } finally {
            session.close();
        }
        return null;
    }
}
