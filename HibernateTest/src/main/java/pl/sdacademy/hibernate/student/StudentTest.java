package pl.sdacademy.hibernate.student;

import org.hibernate.*;
import pl.sdacademy.hibernate.utils.SessionFactoryManager;

import javax.persistence.*;
import java.util.Map;

/**
 * Created by Paweł on 2016-03-20.
 */
@Entity
@Table(name = "student_test")
public class StudentTest {

	@Id
	@SequenceGenerator(name = "pk_student_test", sequenceName = "student_test_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_student_test")
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "student_id")
	private Student student;

	@ManyToOne(optional = false)
	@JoinColumn(name = "test_id")
	private Test test;

	@Column(nullable = false, name = "score")
	private double score;

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public static StudentTest save(StudentTest studentTest) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			studentTest.id = null;
			session.save(studentTest);
			transaction.commit();
			return studentTest;
		} catch (Exception e) {
			System.out.println("StudentTest - rollback");
			System.out.println(e.getMessage());
			transaction.rollback();
		} finally {
			System.out.println("ST - close");
			session.close();
		}
		return null;
	}

	public static double getAvgScoreForStudentId(Long studentId) {

		Double score = null;

		String sql = "select avg(st.score) as average from student_test st where st.student_id=" + studentId;
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		SQLQuery query = session.createSQLQuery(sql);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		Object object = query.uniqueResult();
		Map map = (Map) object;
		System.out.println("MAP: " + map);
		score = Double.valueOf(map.get("average").toString());

		return score;
	}
}
