package pl.sdacademy.hibernate.utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import pl.sdacademy.hibernate.school.School;
import pl.sdacademy.hibernate.school.SchoolClass;
import pl.sdacademy.hibernate.student.Student;
import pl.sdacademy.hibernate.student.StudentTest;
import pl.sdacademy.hibernate.student.Test;

/**
 * Created by Paweł on 2016-03-20.
 */
public class SessionFactoryManager {

	private static SessionFactory sessionFactory = null;

	private static void prepareConnection() {

		Configuration configuration = new Configuration().configure();
		configuration.addAnnotatedClass(School.class);
		configuration.addAnnotatedClass(SchoolClass.class);
		configuration.addAnnotatedClass(Student.class);
		configuration.addAnnotatedClass(StudentTest.class);
		configuration.addAnnotatedClass(Test.class);

		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		sessionFactory = configuration.buildSessionFactory(builder.build());
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			prepareConnection();
		}
		return sessionFactory;
	}

}
