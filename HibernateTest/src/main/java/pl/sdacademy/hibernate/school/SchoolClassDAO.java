package pl.sdacademy.hibernate.school;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sdacademy.hibernate.utils.SessionFactoryManager;

import java.util.List;

/**
 *
 */
public class SchoolClassDAO {

	private static final Logger log = Logger.getLogger(SchoolClassDAO.class);

	private final SessionFactory sessionFactory;

	public SchoolClassDAO() {
		sessionFactory = SessionFactoryManager.getSessionFactory();
	}

	public List<SchoolClass> findBySchoolId(long schoolId) {
		Session session = sessionFactory.openSession();
		String hql = "from SchoolClass sc where sc.school.id = :schoolId";
		org.hibernate.Query query = session.createQuery(hql);
		query.setParameter("schoolId", schoolId);
		List<SchoolClass> results = (List<SchoolClass>) query.list();
		session.close();
		return results;
	}

	public SchoolClass findById(long id) {
		Session session = sessionFactory.openSession();
		SchoolClass schoolClass = session.get(SchoolClass.class, id);
		session.close();
		return schoolClass;
	}

	public SchoolClass save(SchoolClass schoolClass) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(schoolClass);
			transaction.commit();
			return schoolClass;
		} catch (Exception e) {
			log.error("SchoolClass::save() - Rollback: " + e.getMessage(), e);
			transaction.rollback();
		} finally {
			session.close();
		}
		return null;
	}

	public void update(SchoolClass schoolClass) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(schoolClass);
			transaction.commit();
		} catch (Exception e) {
			log.error("SchoolClass::update() - Rollback: " + e.getMessage(), e);
			transaction.rollback();
		} finally {
			session.close();
		}
	}

	public void delete(SchoolClass schoolClass) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(schoolClass);
			transaction.commit();
		} catch (Exception e) {
			log.error("SchoolClass::delete() - Rollback: " + e.getMessage(), e);
			transaction.rollback();
		} finally {
			session.close();
		}
	}
}
