package pl.sdacademy.hibernate.school;

import pl.sdacademy.hibernate.student.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sdacademy.hibernate.utils.SessionFactoryManager;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Paweł on 2016-03-20.
 */
@Entity
@Table(name = "school_class")
public class SchoolClass {

	@Id
	@SequenceGenerator(name = "pk_school_class", sequenceName = "school_class_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_school_class")
	private Long id;

	@Column(name = "level", nullable = false)
	private Integer level;

	@Column(name = "name", nullable = false)
	private String name;

	@ManyToOne(optional = false)
	private School school;

	@OneToMany(mappedBy = "schoolClass")
	private List<Student> studentList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<Student> getStudentList() {
		return studentList;
	}

	public void setStudentList(List<Student> studentList) {
		this.studentList = studentList;
	}

	@Override
	public String toString() {
		return "SchoolClass{" +
				"id=" + id +
				", level=" + level +
				", name='" + name + '\'' +
				", school=" + school +
				'}';
	}

	public static List<SchoolClass> findBySchoolId(long schoolId) {

		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();

		String hql = "from SchoolClass sc where sc.school.id = :schoolId";
		org.hibernate.Query query = session.createQuery(hql);
		query.setParameter("schoolId", schoolId);
		List<SchoolClass> results = (List<SchoolClass>) query.list();
		session.close();
		return results;
	}

	public static SchoolClass findById(long id) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		SchoolClass schoolClass = session.get(SchoolClass.class, id);
		session.close();
		return schoolClass;
	}

	public static SchoolClass save(SchoolClass schoolClass) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			schoolClass.id = null;
			session.save(schoolClass);
			transaction.commit();
			return schoolClass;
		} catch (Exception e) {
			System.out.println("SchoolClass - rollback");
			System.out.println(e.getMessage());
			transaction.rollback();
		} finally {
			session.close();
		}
		return null;
	}

	public static void update(SchoolClass schoolClass) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(schoolClass);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		} finally {
			session.close();
		}
	}

	public static void delete(SchoolClass schoolClass) {
		SessionFactory factory = SessionFactoryManager.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(schoolClass);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		} finally {
			session.close();
		}
	}
}
