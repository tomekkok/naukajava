package pl.sdacademy.hibernate.school;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sdacademy.hibernate.utils.SessionFactoryManager;

/**
 *
 */
public class SchoolDAO {

	private static final Logger log = Logger.getLogger(SchoolDAO.class);

	private final SessionFactory sessionFactory;

	public SchoolDAO() {
		sessionFactory = SessionFactoryManager.getSessionFactory();
	}

	public School findById(long id) {
		Session session = sessionFactory.openSession();
		School school = null;
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			school = session.get(School.class, id);
			if (null != school) {
				// inicjalizacja kolekcji
				Hibernate.initialize(school.getSchoolClassList());
			}
		} catch (Exception e) {
			log.error("School::findById() - Rollback: " + e.getMessage(), e);
			transaction.rollback();
		} finally {
			session.close();
		}
		return school;
	}

	public School save(School school) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.persist(school);
			transaction.commit();
			return school;
		} catch (Exception e) {
			log.error("School::save() - Rollback: " + e.getMessage(), e);
			transaction.rollback();
		} finally {
			session.close();
		}
		return null;
	}

	public void update(School school) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(school);
			transaction.commit();
		} catch (Exception e) {
			log.error("School::update() - Rollback: " + e.getMessage(), e);
			transaction.rollback();
		} finally {
			session.close();
		}
	}

	public void delete(School school) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(school);
			transaction.commit();
		} catch (Exception e) {
			log.error("School::delete() - Rollback: " + e.getMessage(), e);
			transaction.rollback();
		} finally {
			session.close();
		}
	}
}
