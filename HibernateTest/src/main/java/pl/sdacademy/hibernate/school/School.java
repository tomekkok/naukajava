package pl.sdacademy.hibernate.school;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Paweł on 2016-03-20.
 */
@Entity
@Table(name = "school")
public class School {

	@Id
	@SequenceGenerator(name = "pk_school", sequenceName = "school_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_school")
	private Long id;

	@Column(name = "level", nullable = false)
	private Integer level;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "country", nullable = false)
	private String country;

	@Column(name = "city", nullable = false)
	private String city;

	@Column(name = "zip_code", nullable = false)
	private String zipCode;

	@Column(name = "street", nullable = false)
	private String street;

	@OneToMany(mappedBy = "school")
	private List<SchoolClass> schoolClassList;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public List<SchoolClass> getSchoolClassList() {
		return schoolClassList;
	}

	public void setSchoolClassList(List<SchoolClass> schoolClassList) {
		this.schoolClassList = schoolClassList;
	}

	@Override
	public String toString() {
		return "School{" +
				", street='" + street + '\'' +
				", zipCode='" + zipCode + '\'' +
				", city='" + city + '\'' +
				", country='" + country + '\'' +
				", name='" + name + '\'' +
				", level=" + level +
				", id=" + id +
				'}';
	}


}
