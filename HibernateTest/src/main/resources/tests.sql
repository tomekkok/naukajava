--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: play_evolutions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE play_evolutions (
    id integer NOT NULL,
    hash character varying(255) NOT NULL,
    applied_at timestamp without time zone NOT NULL,
    apply_script text,
    revert_script text,
    state character varying(255),
    last_problem text
);


ALTER TABLE play_evolutions OWNER TO postgres;

--
-- Name: school; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE school (
    id bigint NOT NULL,
    level integer NOT NULL,
    name character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    zip_code character varying(255) NOT NULL,
    street character varying(255) NOT NULL
);


ALTER TABLE school OWNER TO postgres;

--
-- Name: school_class; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE school_class (
    id bigint NOT NULL,
    level integer NOT NULL,
    name character varying(255) NOT NULL,
    school_id bigint NOT NULL
);


ALTER TABLE school_class OWNER TO postgres;

--
-- Name: school_class_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE school_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE school_class_id_seq OWNER TO postgres;

--
-- Name: school_class_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE school_class_id_seq OWNED BY school_class.id;


--
-- Name: school_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE school_id_seq OWNER TO postgres;

--
-- Name: school_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE school_id_seq OWNED BY school.id;


--
-- Name: student; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    reg_number integer NOT NULL,
    school_class_id bigint NOT NULL
);


ALTER TABLE student OWNER TO postgres;

--
-- Name: student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE student_id_seq OWNER TO postgres;

--
-- Name: student_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_id_seq OWNED BY student.id;


--
-- Name: student_test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student_test (
    id bigint NOT NULL,
    student_id bigint NOT NULL,
    test_id bigint NOT NULL
);


ALTER TABLE student_test OWNER TO postgres;

--
-- Name: student_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE student_test_id_seq OWNER TO postgres;

--
-- Name: student_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_test_id_seq OWNED BY student_test.id;


--
-- Name: test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test (
    id bigint NOT NULL,
    end_date timestamp without time zone NOT NULL
);


ALTER TABLE test OWNER TO postgres;

--
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_id_seq OWNER TO postgres;

--
-- Name: test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_id_seq OWNED BY test.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY school ALTER COLUMN id SET DEFAULT nextval('school_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY school_class ALTER COLUMN id SET DEFAULT nextval('school_class_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student ALTER COLUMN id SET DEFAULT nextval('student_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test ALTER COLUMN id SET DEFAULT nextval('student_test_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test ALTER COLUMN id SET DEFAULT nextval('test_id_seq'::regclass);


--
-- Data for Name: play_evolutions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY play_evolutions (id, hash, applied_at, apply_script, revert_script, state, last_problem) FROM stdin;
1	732661afa2eced09c1bfabb1d26178fd83de2b1e	2016-03-06 00:00:00	create table school (\nid                        bigserial not null,\nlevel                     integer not null,\nname                      varchar(255) not null,\ncountry                   varchar(255) not null,\ncity                      varchar(255) not null,\nzip_code                  varchar(255) not null,\nstreet                    varchar(255) not null,\nconstraint pk_school primary key (id))\n;\n\ncreate table school_class (\nid                        bigserial not null,\nlevel                     integer not null,\nname                      varchar(255) not null,\nschool_id                 bigint not null,\nconstraint pk_school_class primary key (id))\n;\n\ncreate table student (\nid                        bigserial not null,\nfirst_name                varchar(255) not null,\nlast_name                 varchar(255) not null,\nreg_number                integer not null,\nschool_class_id           bigint not null,\nconstraint pk_student primary key (id))\n;\n\ncreate table student_test (\nid                        bigserial not null,\nstudent_id                bigint not null,\ntest_id                   bigint not null,\nconstraint pk_student_test primary key (id))\n;\n\ncreate table test (\nid                        bigserial not null,\nend_date                  timestamp not null,\nconstraint pk_test primary key (id))\n;\n\nalter table school_class add constraint fk_school_class_school_1 foreign key (school_id) references school (id);\ncreate index ix_school_class_school_1 on school_class (school_id);\nalter table student add constraint fk_student_schoolClass_2 foreign key (school_class_id) references school_class (id);\ncreate index ix_student_schoolClass_2 on student (school_class_id);\nalter table student_test add constraint fk_student_test_student_3 foreign key (student_id) references student (id);\ncreate index ix_student_test_student_3 on student_test (student_id);\nalter table student_test add constraint fk_student_test_test_4 foreign key (test_id) references test (id);\ncreate index ix_student_test_test_4 on student_test (test_id);	drop table if exists school cascade;\n\ndrop table if exists school_class cascade;\n\ndrop table if exists student cascade;\n\ndrop table if exists student_test cascade;\n\ndrop table if exists test cascade;	applied	
2	da39a3ee5e6b4b0d3255bfef95601890afd80709	2016-03-06 00:00:00			applied	
\.


--
-- Data for Name: school; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY school (id, level, name, country, city, zip_code, street) FROM stdin;
\.


--
-- Data for Name: school_class; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY school_class (id, level, name, school_id) FROM stdin;
\.


--
-- Name: school_class_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('school_class_id_seq', 1, false);


--
-- Name: school_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('school_id_seq', 1, false);


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY student (id, first_name, last_name, reg_number, school_class_id) FROM stdin;
\.


--
-- Name: student_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_id_seq', 1, false);


--
-- Data for Name: student_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY student_test (id, student_id, test_id) FROM stdin;
\.


--
-- Name: student_test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_test_id_seq', 1, false);


--
-- Data for Name: test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY test (id, end_date) FROM stdin;
\.


--
-- Name: test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('test_id_seq', 1, false);


--
-- Name: pk_school; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY school
    ADD CONSTRAINT pk_school PRIMARY KEY (id);


--
-- Name: pk_school_class; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY school_class
    ADD CONSTRAINT pk_school_class PRIMARY KEY (id);


--
-- Name: pk_student; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT pk_student PRIMARY KEY (id);


--
-- Name: pk_student_test; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_test
    ADD CONSTRAINT pk_student_test PRIMARY KEY (id);


--
-- Name: pk_test; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test
    ADD CONSTRAINT pk_test PRIMARY KEY (id);


--
-- Name: play_evolutions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY play_evolutions
    ADD CONSTRAINT play_evolutions_pkey PRIMARY KEY (id);


--
-- Name: ix_school_class_school_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_school_class_school_1 ON school_class USING btree (school_id);


--
-- Name: ix_student_schoolclass_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_student_schoolclass_2 ON student USING btree (school_class_id);


--
-- Name: ix_student_test_student_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_student_test_student_3 ON student_test USING btree (student_id);


--
-- Name: ix_student_test_test_4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_student_test_test_4 ON student_test USING btree (test_id);


--
-- Name: fk_school_class_school_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY school_class
    ADD CONSTRAINT fk_school_class_school_1 FOREIGN KEY (school_id) REFERENCES school(id);


--
-- Name: fk_student_schoolclass_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student
    ADD CONSTRAINT fk_student_schoolclass_2 FOREIGN KEY (school_class_id) REFERENCES school_class(id);


--
-- Name: fk_student_test_student_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test
    ADD CONSTRAINT fk_student_test_student_3 FOREIGN KEY (student_id) REFERENCES student(id);


--
-- Name: fk_student_test_test_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_test
    ADD CONSTRAINT fk_student_test_test_4 FOREIGN KEY (test_id) REFERENCES test(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

