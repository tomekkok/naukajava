package tutorial;

import javafx.beans.binding.NumberBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.util.converter.NumberStringConverter;

public class Example1 {

	public static void main(String[] args) {

		IntegerProperty a = new SimpleIntegerProperty();
		IntegerProperty b = new SimpleIntegerProperty();

		NumberBinding c = a.add(b);

		a.set(2);
		b.set(3);

		System.out.println(c.getValue());

		c.addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

				System.out.println("c= " + newValue);

			}
		});

		a.set(20);
		b.set(13);

		IntegerProperty d = new SimpleIntegerProperty();
		d.bind(c);

		System.out.println("d= " + d.getValue());

		StringProperty text = new SimpleStringProperty();

		text.bindBidirectional(a, new NumberStringConverter());

		text.set("9");
	}

}
