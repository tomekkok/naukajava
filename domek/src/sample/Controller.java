package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private House house = new HouseTest();

    @FXML
    private ChoiceBox<House.HeatingLevel> heatingChoiceBox;

    @FXML
    private CheckBox windowsCloseCheckBox;

    @FXML
    void showOptions(ActionEvent event) {
        house.showOptions();
    }

    @FXML
    void showUpdates(ActionEvent event) {
        house.showUpdates();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        heatingChoiceBox.getItems().addAll(House.HeatingLevel.values());

        windowsCloseCheckBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                house.setWindowsClosed(windowsCloseCheckBox.isSelected());
            }
        });

        heatingChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<House.HeatingLevel>() {
            @Override
            public void changed(ObservableValue<? extends House.HeatingLevel> observable, House.HeatingLevel oldValue, House.HeatingLevel newValue) {
                house.setHeatingLevel(heatingChoiceBox.getValue());
            }
        });
    }
}
