package sample;

public class HouseTest implements House{

    @Override
    public void showUpdates() {
        System.out.println("showUpdates");
    }

    @Override
    public void showOptions() {
        System.out.println("showOptions");
    }

    @Override
    public void setWindowsClosed(boolean closed) {
        System.out.println("closed = " + closed);
    }

    @Override
    public void setHeatingLevel(HeatingLevel level) {
        System.out.println("heating level = " + level.name());
    }
}
