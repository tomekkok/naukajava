<%@ page import="java.util.Date" %>
<%--
  Created by IntelliJ IDEA.
  User: RENT
  Date: 2016-05-09
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Mechanizm Sesji</title>
</head>
<body>
    <p>Session: <%=session.getId()%></p>
    <p>Session: <%
        Date data = new Date(session.getCreationTime());
        out.println(data);
    %></p>
    <p>Session is new: <%=session.isNew()%></p>
    <%--<%session.invalidate();%>--%>
</body>
</html>
