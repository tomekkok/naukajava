<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cookie</title>
</head>
<body>
<%
    Cookie cookie = new Cookie("moje_ciastko", "wartosc_ciastka");
    cookie.setMaxAge(60 * 60 * 24);
    response.addCookie(cookie);
    out.println("ustawilem ciastko");
    Cookie[] cookies = request.getCookies();
    for (Cookie c : cookies) {
        %><p>Cookie name: <b><%=c.getName()%></b>,<br />
        Cookie value: <b><%=c.getValue()%></b></p><%
    }
%>

</body>
</html>
