import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by RENT on 2016-05-11.
 */


@WebFilter(filterName="HelloWorldFilter", urlPatterns = "/hello")
public class HelloWorldFilter implements Filter {


    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        PrintWriter wr = servletResponse.getWriter();
        wr.println("HelloWorld from Filter [before]");

        RequestDispatcher rd = servletRequest.getRequestDispatcher("index.jsp");
                rd.include(servletRequest, servletResponse);

        filterChain.doFilter(servletRequest, servletResponse);

        wr.println("HelloWorld from Filter [after]");
    }

    public void destroy() {

    }
}
