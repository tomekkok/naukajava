import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//@WebServlet(name = "Hello World", value = "/hello")
public class HelloWorld extends HttpServlet{

    private String who;
    private int times;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        who = config.getInitParameter("who");
        //times = Integer.parseInt(config.getInitParameter("times"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        times = Integer.parseInt(getServletConfig().getInitParameter("times"));
        PrintWriter pw = resp.getWriter();
        for (int i = 0; i < times; i++) {
            pw.println("Hello " + who +" from servlet!");
        }


    }
}